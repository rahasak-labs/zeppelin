# zeppelin

## k-means clustering with apache spark and zeppelin notbook on docker

- read from [here](https://medium.com/rahasak/k-means-clustering-with-apache-spark-and-zeppelin-notebook-on-docker-4ed2db66c3c8)


## anomaly detection with isolation forest spark scala

- read from [here](https://medium.com/rahasak/anomaly-detection-with-isolation-forest-spark-scala-8d8b5f36c47c)
